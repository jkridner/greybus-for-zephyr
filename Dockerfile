FROM debian:bullseye as zephyr-build-env

RUN apt update && apt install -y git tree
COPY setup.sh .
RUN ./setup.sh

